//
//  HomeVC.swift
//  Dealz Chain
//
//  Created by Saif on 10/02/22.
//

import UIKit
import ImageSlideshow
class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let titel = ["Real Estate","Cars","Yachts","Watches"]
    
    let images = [[UIImage(named: "img-1"),UIImage(named: "img-2"),UIImage(named: "img-1"),UIImage(named: "img-2")],
                  
                  [UIImage(named: "img-3"),UIImage(named: "img-4"),UIImage(named: "img-3"),UIImage(named: "img-4")],
                  
                   [UIImage(named: "img-5"),UIImage(named: "img-6"),UIImage(named: "img-5"),UIImage(named: "img-6")],
                  
                   [UIImage(named: "img-7"),UIImage(named: "img-8"),UIImage(named: "img-7"),UIImage(named: "img-8")]]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return 4
    }
    
    @IBOutlet weak var tableViewHeightContriant: NSLayoutConstraint!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath) as! mainCell
        cell.titeltext.text = titel[indexPath.row]
        cell.imagearray = images[indexPath.row] as? [UIImage]
        cell.calldelegate()
        
        return cell
    }
    
    
    let heightForRowAt:CGFloat = 360
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return heightForRowAt
        
    }
    
    

    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = appsCyanColor
        pageIndicator.pageIndicatorTintColor = UIColor.gray
        slideshow.pageIndicator = pageIndicator
        
        slideshow.setImageInputs([
            ImageSource(image: UIImage(named: "banner")!),
            ImageSource(image: UIImage(named: "banner")!),
            ImageSource(image: UIImage(named: "banner")!),
        ])
        
        
        tableViewHeightContriant.constant = CGFloat(titel.count) * heightForRowAt
        tableView.layoutIfNeeded()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()

        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
