//
//  CollectionCell.swift
//  Dealz Chain
//
//  Created by Saif on 10/02/22.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageItem: UIImageView!{
        didSet{
            imageItem.layer.borderWidth = 1
            imageItem.clipsToBounds = true
            imageItem.layer.borderColor = appsCyanColor.cgColor
        }
    }
    @IBOutlet weak var textItem: UILabel!
    
    @IBOutlet weak var priceItem: UILabel!{
        didSet{
//            
//            let mainString = "Price: 4.89 ETH"
//            let stringToColor = "Price:"
//            
//            let range = (mainString as NSString).range(of: stringToColor)
//
//            let mutableAttributedString = NSMutableAttributedString.init(string: mainString)
//            mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
//            priceItem.attributedText = mutableAttributedString
        }
    }
    
   
}
